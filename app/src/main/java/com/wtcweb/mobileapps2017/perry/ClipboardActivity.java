package com.wtcweb.mobileapps2017.perry;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wtcweb.mobileapps2017.R;

public class ClipboardActivity extends AppCompatActivity {

    Button btnNotification;
    EditText txtCopy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clipboard);

        btnNotification = (Button)findViewById(R.id.btnNotification);
        txtCopy = (EditText)findViewById(R.id.txtCopy);


        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("copied text",txtCopy.getText().toString());
                clipboard.setPrimaryClip(clip);


                Context context = getApplicationContext();
                CharSequence text = "'"+txtCopy.getText().toString() + "' copied to clipboard";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });


    }
}
